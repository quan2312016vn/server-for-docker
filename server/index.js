const express = require("express");
const app = express();
const mongoose = require("mongoose");
const fs = require("fs");
const morgan = require("morgan");

const {
    MONGO_URI,
    QUERY_ID
} = process.env;

mongoose.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.on("open", () => {
    fs.appendFile("./logs/mongo.log", "Connected db", () => {});
});

mongoose.connection.on("error", (e) => {
    fs.appendFile("./logs/mongo.log", e.toString(), () => {});
});

const MyModel = mongoose.model("server", new mongoose.Schema({ _id: Number, content: String }));

app.set("json spaces", 2);

app.use(morgan("dev", { 
    stream: fs.createWriteStream("./logs/request.log", { flags: "a" })
}));

app.get("/", async (req, res) => {
    const model = await MyModel.findOne({ _id: Number(QUERY_ID) });
    res.json({
        metadata: "Data from DB",
        model
    });
});

app.listen(3000, () => console.log("running on port 3000"));